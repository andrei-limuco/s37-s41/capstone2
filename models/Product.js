const mongoose = require ("mongoose");

const productSchema = new mongoose.Schema({

   name: {
        type: String,
        required: [true, "Name is required"],
        unique: true
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    // img: {
    //     type: String,
    //     required: [true, "Image is required"],
    //     unique: true
    // },
    // categories: {
    //     type: Array
    // },
    // size: {
    //     type: String
    // },
    // color: {
    //     type: String
    // },
    price: {
        type: Number,
        required: [true, "Price is required"],
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Product", productSchema);