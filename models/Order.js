const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required"],
	},
	product: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"],
			},
			quantity: {
				type: Number,
				// default: 1,
				required: [true, "Quantity is required"],
			},
			price: {
				type: Number,
				required: [true, "Price is required"],
			},
		},
	],
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"],
	},
	purchasedOn: {
		type: Date,
		default: new Date(),
	},
	status: {
		type: String,
		default: "pending",
	},
});

module.exports = mongoose.model("Order", orderSchema);
