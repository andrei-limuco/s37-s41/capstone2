const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const ejs = require("ejs");
const path = require("path");
const port = 4001;
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const User = require("./models/User");

const app = express();

//Frontend -------------------------------------------------------------
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));
app.use(express.static(path.join(__dirname, "./public")));

app.get("/", (req, res) => {
	res.render("home");
});

app.get("/landing-page", (req, res) => {
	res.render("landingPage");
});

app.get("/users/register", (req, res) => {
	res.render("signup");
});

app.get("/users/login", (req, res) => {
	res.render("login");
});

app.get("/products", (req, res) => {
	res.render("shopAll");
});

app.get("/products/air-jordan-1", (req, res) => {
	res.render("airJordan1");
});

app.get("/products/nike-air-force-1", (req, res) => {
	res.render("nikeAirForce1");
});

app.get("/products/nike-dunk", (req, res) => {
	res.render("nikeDunk");
});

app.get("/products/622adcbc3c65bf6bfe53e5ca", (req, res) => {
	res.render("airJordan1LowBredToe");
});
// -------------------------------------------------------------

mongoose.connect(
	"mongodb+srv://admin:admin123@course-booking.iha4o.mongodb.net/Capstone2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
});
