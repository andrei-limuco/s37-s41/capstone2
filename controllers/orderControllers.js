const Order = require("../models/Order");

//Order Creation
module.exports.createOrder = (data) => {
	let newOrder = new Order({
		userId: data.userId,
		product: [
			{
				productId: data.productId,
				quantity: data.quantity,
				price: data.price,
			},
		],
		totalAmount: data.quantity * data.price,
	});

	return newOrder.save().then((order, error) => {
		if (error) {
			return false;
		} else {
			return order;
		}
	});
};

//Retrieve all orders
module.exports.viewAllOrders = (data) => {
	return Order.find({}).then((orders) => {
		if (data.isAdmin) {
			return orders;
		} else {
			return false;
		}
	});
};

//Retrieve Authenticated User’s Orders
module.exports.viewOrdersFromUser = (data) => {
	return Order.findOne({ userId: data.userId }).then((order) => {
		if (data.isAdmin) {
			return false;
		} else {
			return order;
		}
	});
};
