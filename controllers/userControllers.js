const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then((result) => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	});
};

//User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			);

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) };
			} else {
				return false;
			}
		}
	});
};

//Set logged in user as admin
module.exports.setAsAdmin = (data) => {
	// console.log(data);

	return User.findById(data.setUserAsAdmin).then((result, error) => {
		// console.log(result);

		if (data.isAdmin) {
			result.isAdmin = true;
			// console.log(result);

			return result.save().then((updatedUser, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			});
		} else {
			return "Unauthorized";
		}
	});
};
